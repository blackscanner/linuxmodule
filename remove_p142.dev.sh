#!/bin/sh
set -e #important for rmmod fails 
devicePath="/dev/p142_module"

 rmmod "p124_module"

# I had some problems with udev,
# the device manager. If you don't
# have a problem with it then this 
# shouldn't execute.
if [ -c "$devicePath" ];
then
	 rm /dev/p142_module
fi
