#!/bin/sh
set -e #important for if insmod fails
devicePath="/dev/p142_module"

 insmod /home/greg/Documents/cmpe142/learnLinuxModules/linuxmodule/p142_module.ko

# I had some problems with udev,
# the device manager. If you don't
# have a problem with it then this 
# shouldn't execute.
if [ ! -c "$devicePath" ];
then
	 mknod /dev/p142_module c 235 5 	#The majore number is the same one as defined in the macro
						#in the p142_module.c (MAJOR_NUMBER), the minor number 5 is 
						#arbitrary
fi
echo "Added link /dev/p142_module"
