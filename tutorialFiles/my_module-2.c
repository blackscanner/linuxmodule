#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

static int __init initalize(void)
{
	printk(KERN_INFO "hello, world 2\n");
	return 0;
}

static void __exit close(void)
{
	printk(KERN_INFO "Goodbye, world 2\n");
}

module_init(initalize);
module_exit(close);
