#ifndef PROC_TALK
#define PROC_TALK
#define debug 1
#include <linux/ioctl.h>

/* Char (and block) drivers have a major number and a minor number,
 * for this driver, an unassigned number will be used
 */
#define MAJOR_NUMBER 235//should be a free unassigned major number
#define WRITE_TO_BUFFER__IOCTL _IOR(MAJOR_NUMBER, 0, char*)
/* Used for tellin the driver that there is a user level process
 * requsting to write the the buffer
 */

#define READ_FROM_BUFFER__IOCTL _IOR(MAJOR_NUMBER, 1, char*)
/* For processes to read from the buffer
 */

#define READ_FROM_NTHBYTE__IOCTL _IOR(MAJOR_NUMBER, 2, int)
/* Incase we don't wish to read the entire buffer, used to return
 * char at a given index
 */

#define GET_BUFFER_LENGTH__IOCTL _IOR(MAJOR_NUMBER, 3, void)
/* So that we we can tell the read functions what the maximum number
 * bytes the device can read from the buffer
 */
#define MOD_NAME "p142_module"
#endif
