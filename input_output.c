#include "p142_module.h"
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*message 	The message to put into the buffer,
 * (char*)	the maximum amount that the buffer can 
 *		handle is 256 bytes.
 *
 *-return-	1: Then ioctl failed
 * (int)	0: Then ioctl succeded
 */
int send_message(int file_dscp, char* message)
{
#ifdef debug
	printf("send_messag(%d.%s) (usr_l)\n",file_dscp,message);
#endif
	if(!ioctl(file_dscp, WRITE_TO_BUFFER__IOCTL, message)) return 1;
	else return 0;
}

/*-return-	Either a dymnamic character array or NULL. 
 * (char*)	NULL indicates ioctl failed.
 */
char * request_message(int file_dscp)
{
#ifdef debug
	printf("request_message(%d) (usr_l)\n",file_dscp);
#endif
	/* Warning message about this is ok to ignore */
	char * message;
	if(!ioctl(file_dscp, READ_FROM_BUFFER__IOCTL, message))	return NULL;
	else return message;
}
<<<<<<< HEAD
=======

int main()
{
	char * file_path = malloc(strlen("/dev/") + strlen(MOD_NAME) + 1);
	strcat(strcpy(file_path,"/dev/") , MOD_NAME);

	int file_dscp = open(file_path, O_RDONLY);
	if(file_dscp == -1){
		printf("The driver couldn't be opened\n");
		exit(-1);
	} 
		
	char message[] = "This is the message";
	if(send_message(file_dscp, message)){
		printf("Message could not be sent\n");
		close(file_dscp);
		exit(-1);
	}
	char * mes = request_message(file_dscp);
	printf("Message recieved %s\n", mes);
	
	
	close(file_dscp);
	return 0;
}
>>>>>>> parent of 65c07ba... Everything works for now I think.  We haven't met all the requirements of the project but I think we are 70% of the way there. IMPORTANT: to make the files, run sudo sh make.sh and not make, it enables the use of modprobe with this module.  To clean up the files, you still need to run make clean
