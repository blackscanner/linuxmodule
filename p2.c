int main()
{
	char * file_path = malloc(sizeof(char) * (strlen("/dev/") + strlen(MOD_NAME) + 1));
	strcat(strcpy(file_path,"/dev/") , MOD_NAME);

	char * mes = request_message(file_dscp);
#ifdef debug
	/*Incase things break*/
	char * temp = mes ? realloc(mes, sizeof(char) * (MAX_LENGTH + 1)) : malloc(sizeof(char));
	int i;
	for(i = 0; i < MAX_LENGTH; i++) temp[i] = mes[i];
	temp[MAX_LENGTH] = '\0';
	mes = temp;
#endif
	printf("Message recieved %s\n", mes);
	free(mes);
	
	close(file_dscp);
	return 0;
}
