int main()
{
	char * file_path = malloc(sizeof(char) * (strlen("/dev/") + strlen(MOD_NAME) + 1));
	strcat(strcpy(file_path,"/dev/") , MOD_NAME);

	int file_dscp = open(file_path, O_RDONLY);
	if(file_dscp == -1){
		printf("The driver couldn't be opened\n");
		exit(-1);
	} 
		
	char message[] = "This is the message";
	if(!send_message(file_dscp, message)){
		printf("Message could not be sent\n");
		close(file_dscp);
		exit(-1);
	}

	close(file_dscp);
}
