#include <linux/kernel.h> 
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include "p142_module.h"				/* This is the file that 
							   manages the talking between
							   procees
							*/
#define MAX_LENGTH  256					/* Just an arbitrary number*/


static int device_occupied = 0;				/* If zero, not occupied.
							   If 1, then occupied
							*/
static char p142buffer [MAX_LENGTH];			/* The buffer of the module,
							   The meat and potatoes
							*/
static char * messageptr;				/* Did the file reading/writing 
							   complete writing to this module
							*/

/* For when a process open the device */
static int device_open(struct inode *inodeptr, struct file *fileptr)
{
#ifdef debug
	printk(KERN_ALERT "%s was successfully opened\n", MOD_NAME);
#endif
	//mutex so that two processes should not check this
	//at the same time
	if(device_occupied){
#ifdef debug
		printk(KERN_ALERT "but was occupied by another process\n");
#endif
		return -EBUSY;
	}
	
	device_occupied++;
	 
	messageptr = p142buffer;
	return 0;//for success
}

/* For when a process leaves the device */
static int device_release(struct inode *inodeptr, struct file *fileptr)
{
#ifdef debug
	printk(KERN_ALERT "%s device was successfully released\n", MOD_NAME);
#endif
	//decrement the device_occupied amount
	device_occupied--;

	return 0;//for success
}

/* Reading from the module
 * char __user *buffer is a buffer to put the contents of this module's buffer
 */
static ssize_t device_read(struct file *fileptr, char __user * ubuffer, size_t length, loff_t * offset)
{
	int bytes_read = 0;
#ifdef debug
	char temp[MAX_LENGTH + 1];
	int i;
	printk(KERN_ALERT "A Process is reading from device %s\n", MOD_NAME);
	for(i = 0; i < MAX_LENGTH; i++) temp[i] = p142buffer[i];
	
	//make sure that %s can read the character array as a string
	temp[MAX_LENGTH] = '\0';	
	printk(KERN_ALERT "device_read(%p, %s, %d)\n", fileptr, temp, length);
#endif
	while(bytes_read < length && bytes_read < MAX_LENGTH)
	{
		//converts kernel level buffer data to the user level buffer
		put_user(*(messageptr++), ubuffer++);
		bytes_read++;
	}
	return bytes_read;
}

/* Writing to the driver */
static ssize_t device_write(struct file *fileptr, const char __user * ubuffer, size_t length, loff_t * offset)
{
	int i;
#ifdef debug
	printk(KERN_ALERT "device_write(%p, %s, %d)\n", fileptr, ubuffer, length);
#endif
	for(i = 0; i < length && i < MAX_LENGTH; i++)
	{
		get_user(p142buffer[i], ubuffer + i);
	}
	messageptr = p142buffer;
#ifdef debug
	printk(KERN_ALERT "driver buffer is now: \"%s\"\n", p142buffer);
#endif
	return i;
}

/* Device Input/Output control
 * Used to manage the connection between one process that reads/writes to the buffer in this module
 */
long device_ioctrl(struct file *fileptr, unsigned int ioctrl_type, unsigned long ioctrl_message)
{
	switch(ioctrl_type){
		case WRITE_TO_BUFFER__IOCTL:
			//we don't know the actual length of the message, but that
			//is ok since read/write processes could handle that (with some delimiter)
			
			device_write(fileptr, (char*) ioctrl_message, MAX_LENGTH, 0);
		break;
		case READ_FROM_BUFFER__IOCTL:
			//get the message from the buffer
			device_read(fileptr, (char*) ioctrl_message, MAX_LENGTH, 0);
		break;
	}
	return 0;
}

/* Stuff needed for the module */
struct file_operations Fops = {
	.owner  = THIS_MODULE,
	.read	= device_read,
	.open	= device_open,
	.release= device_release,
	.write	= device_write,
	.unlocked_ioctl	= device_ioctrl,
};

static int __init p124_module_begin(void)
{	
	int reg_device = register_chrdev(MAJOR_NUMBER, MOD_NAME,&Fops);
	if(reg_device < 0)
	{
		printk(KERN_ALERT "Device %s couldn't be registered with major number %d)\n", MOD_NAME, MAJOR_NUMBER);
		return reg_device;
	}
#ifdef debug
	printk(KERN_ALERT "The device %s with major number %d was successfully registered)\n", MOD_NAME, MAJOR_NUMBER);
#endif
	return 0;
}

static void __exit p124_module_end(void)
{
	unregister_chrdev(MAJOR_NUMBER, MOD_NAME);
}

module_init(p124_module_begin);
module_exit(p124_module_end);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Just a simple modlue to handle communication between processes");
