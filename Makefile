obj-m += p142_module.o
obj-m += p1.o
obj-m += p2.o
OUTPUT := output.log
KDIR := /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)
CFILS := input_output.c
COUTPT := input_output.o
 

all:
	echo 'MODULE:' > $(OUTPUT) 
	$(MAKE) -C $(KDIR) M=$(PWD) modules >> $(OUTPUT) 2>&1
	echo '\nPROCESS:' >> $(OUTPUT) 
	gcc -Wall -o $(COUTPT) $(CFILS) >> $(OUTPUT) 2>&1
clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
